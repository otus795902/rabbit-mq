package ru.otus.rabbitmq;

import java.util.List;
import java.util.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@RequestMapping("/message")
public interface MessageUi {

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  Message create(@RequestParam String name);

  @GetMapping("/{id}")
  Message readOne(@PathVariable UUID id);

  @GetMapping
  List<Message> readAll();

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  void delete(@PathVariable UUID id);

  void consume(Message message);
}