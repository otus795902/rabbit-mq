# RABBIT MQ - ПРИМЕР ОТ ОТУС

Функциональность
-------------

Создает производителяи потребителя очереди брокера Rabbit MQ

Инраструктура и технологии
---------------------------

* Java 17
* Springboot
* Spring Web (Tomcat)
* AMQP
* Open API (Swagger)
* Lombok

Использование
------------

### Сборка

<code>mvn clean install</code>

В параметрах конфигурации указать:

<code>ru.otus.rabbitmq.first-queue-name</code> - название очереди

<code>ru.otus.rabbitmq.exchange-binding-key</code> - ключ маршрутизации для попадания в очередь

<code>ru.otus.rabbitmq.message-routing-key</code> - ключ из сообщения для маршрутизации

### Обучение

Теория (Презентация) - https://docs.google.com/presentation/d/1mUAU9Nwywa10i5pJgI80wO6GJE2GXX3F83DUKeBltaA/edit?usp=sharing

Опрос (Google Forms) - https://docs.google.com/forms/d/e/1FAIpQLSd10H7Dc4RXe-RwKe-C4YehdfDGTQj3YoAnZ16F5U7jcmbAFg/viewform?usp=sharing

Практика (Пошаговая инструкция) - https://docs.google.com/document/d/1q-pg_W5fr1D2087115NX_JsAlS7gi5ntQz7jbmG2c4Y/edit?usp=sharing


